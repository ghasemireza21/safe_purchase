App = {
    web3Provider: null,
    contracts: {},

    // init: async function() {
    //     // Load Goods
    //
    //     var data = await $.getJSON('../Goods.json');
    //     var goodsRow = $('#goodsRow');
    //     var goodsTemplate = $('#goodTemplate');
    //
    //     for (i = 0; i < data.length; i++) {
    //         goodsTemplate.find('.panel-title').text(data[i].Goods);
    //         goodsTemplate.find('img').attr('src', data[i].image);
    //         goodsTemplate.find('.item_Goods').text(data[i].Goods);
    //         goodsTemplate.find('.item_Brand').text(data[i].Brand);
    //         goodsTemplate.find('.item_Value').text(data[i].Value);
    //         goodsTemplate.find('.btn-buy').attr('data-id', data[i].id);
    //         goodsRow.append(goodsTemplate.html());
    //
    //     }
    //     return await App.initWeb3();
    // },

    initWeb3: async function() {
        // Modern dapp browsers...
        if (window.ethereum) {
            App.web3Provider = window.ethereum;
            try {
                // Request account access
                //await window.ethereum.enable();
                await window.ethereum.send('eth_requestAccounts');
                console.log("connect succeed");
            } catch (error) {
                // User denied account access...
                console.error("User denied account access")
            }
        }
        // Legacy dapp browsers...
        else if (window.web3) {
            App.web3Provider = window.web3.currentProvider;
        }
        // If no injected web3 instance is detected, fall back to Ganache
        else {

            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
            console.log("Connect to ganache");
        }
        window.web3 = new Web3(App.web3Provider);

        return App.initContract();
    },

    initContract: async function() {

        var smartContractJson = await $.getJSON('SafePurchase.json');
        console.log("initialize contract");
        // Get the necessary contract artifact file and instantiate it with @truffle/contract
        var SafePurchaseArtifact = smartContractJson;
        console.log(SafePurchaseArtifact);
        App.contracts.SafePurchase = TruffleContract(SafePurchaseArtifact);

        // Set the provider for our contract
        App.contracts.SafePurchase.setProvider(App.web3Provider);
        App.deployGoods();
        return App.bindEvents();

        // $.getJSON('SafePurchase.json', function(data) {
        //     console.log("initialize contract");
        //     // Get the necessary contract artifact file and instantiate it with @truffle/contract
        //     var SafePurchaseArtifact = data;
        //     console.log(SafePurchaseArtifact);
        //     App.contracts.SafePurchase = TruffleContract(SafePurchaseArtifact);

        //     // Set the provider for our contract
        //     App.contracts.SafePurchase.setProvider(App.web3Provider);

        //     return App.deployGoods();
        // });
        //return App.bindEvents();
    },

    bindEvents: function() {
        $(document).on('click', '.btn-buy', App.handleBuy);
    },
    deployGoods: async function() {
        console.log("deploying Goods");
        var safePurchaseInstance = await App.contracts.SafePurchase.deployed();
        var data = await $.getJSON('../Goods.json');
        for (i = 0; i < data.length; i++) {
            registerPurchase(i);
        }
        async function registerPurchase(i) {
            //await console.log(data);
            var purchaseInformation = await safePurchaseInstance.getPurchase(data[i].id, { from: this.web3.eth.accounts[0] });
            //if seller address is 0 then purchase is null
            if (purchaseInformation[2] == 0)
                await safePurchaseInstance.registerPurchase(data[i].id, { from: this.web3.eth.accounts[0] });
        }
        // $.getJSON('../Goods.json', function(data) {
        //     for (i = 0; i < data.length; i++) {
        //         f();
        //         async function f() {
        //             //await console.log(data);
        //             await safePurchaseInstance.registerPurchase(data[i].id, { from: this.web3.eth.accounts[0] });
        //         }
        //     }
        // });
    },

    handleBuy: async function(event) {
        console.log("Enter Handle Buy");
        event.preventDefault();

        var goodsId = parseInt($(event.target).data('id'));
        var purchaseInstance;

        web3.eth.getAccounts(async function(error, accounts) {
            if (error) {
                console.log(error);
            }
            try {
                var account = accounts[0];
                purchaseInstance = await App.contracts.SafePurchase.deployed();
                await purchaseInstance.confirmPurchase(goodsId, { from: account });
            } catch (error) {
                console.error(error);
            }
        });
    }

};

$(function() {
    $(window).load(function() {
        App.initWeb3();
    });
});