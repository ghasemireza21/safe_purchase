let web3Provider = null;
let  contracts = {};
let accounts = [];


async function initWeb3() {
    let btn_Connect = document.getElementById("btn_Connect")
    let p_Network = document.getElementById("p_Network")
    // Modern dapp browsers...
    if (window.ethereum) {
        web3Provider = window.ethereum;
        try {
            // Request account access
            accounts = await ethereum.request({ method: 'eth_requestAccounts' });
            const account = accounts[0];

            window.web3 = new Web3(web3Provider);
            btn_Connect.textContent= account;
            getNetwork(p_Network);
            await initContract();
            //console.log("connect succeed");

        } catch (error) {
            // User denied account access...
            console.error("User denied account access")
            console.error(error)
            alertModal("Metamask Connection Error",error.toString())
        }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
        web3Provider = window.web3.currentProvider;
        window.web3 = new Web3(web3Provider);
    }
};


async function initContract (){
    try {
        let SafePurchaseArtifact = await $.getJSON('../smartContract_abi/SafePurchase.json');
        // Get the necessary contract artifact file and instantiate it with @truffle/contract
        contracts.SafePurchase = TruffleContract(SafePurchaseArtifact);
        contracts.SafePurchase.setProvider(web3Provider);
    }
     catch(error){
         alertModal("Wrong Network",error.toString())
     }
}


async function purchaseEvent(eventSender){
    try {
        let goodsCode = parseInt($(eventSender).data('id'));
        let safePurchaseInstance = await contracts.SafePurchase.deployed();
        let result =await safePurchaseInstance.getPurchase(goodsCode);
        if(result[3]==0) {
            try {
                var account = accounts[0];
                let resultTX = await safePurchaseInstance.confirmPurchase(goodsCode.toString(), { from: account, value:(result[0]*2)})
                $.ajax({url: "/purchase/"+goodsCode.toString()+"/"+account.toString(),type: "GET", success: function(result){
                        location.replace("/index")
                    }});
            } catch (error) {
                throw (error);
            }
        }
    }
    catch(error){
        alertModal("Error",error.toString())
    }

}

async function receivedEvent(eventSender){
    try {
        let goodsCode = parseInt($(eventSender).data('id'));
        let safePurchaseInstance = await contracts.SafePurchase.deployed();
        let result =await safePurchaseInstance.getPurchase(goodsCode);
        if(result[3]==1) {
            try {
                var account = accounts[0];
                let resultTX = await safePurchaseInstance.confirmReceived(goodsCode.toString(), { from: account})
                $.ajax({url: "/received/"+goodsCode.toString(),type: "GET", success: function(result){
                        location.replace("/index")
                    }});
            } catch (error) {
                throw (error);
            }
        }
    }
    catch (error) {
        alertModal("Error",error.toString())
    }

}

async function refundEvent(eventSender){
    try {
        let goodsCode = parseInt($(eventSender).data('id'));
        let safePurchaseInstance = await contracts.SafePurchase.deployed();
        let result =await safePurchaseInstance.getPurchase(goodsCode);
        if(result[3]==2) {
            try {
                var account = accounts[0];
                let resultTX = await safePurchaseInstance.refundSeller(goodsCode.toString(), { from: account})
                $.ajax({url: "/refund/"+goodsCode.toString(),type: "GET", success: function(result){
                        location.replace("/index")
                    }});
            } catch (error) {
                throw error;
            }
        }
    }
    catch(error){
        alertModal("Error",error.toString())
    }

}

async function abortEvent(eventSender){
    try {
        let goodsCode = parseInt($(eventSender).data('id'));
        let safePurchaseInstance = await contracts.SafePurchase.deployed();
        let result =await safePurchaseInstance.getPurchase(goodsCode);
        if(result[3]==0) {
            try {
                var account = accounts[0];
                let resultTX = await safePurchaseInstance.abort(goodsCode.toString(), { from: account})
                $.ajax({url: "/refundAbort/"+goodsCode.toString(),type: "GET", success: function(result){
                        location.replace("/index")
                    }});
            } catch (error) {
                throw error
            }
        }
    }
    catch (error) {
        alertModal("Error",error.toString())
    }

}



async function initWeb3_Register() {

    let btn_Connect = document.getElementById("btn_Connect")
    let p_Connection_Status = document.getElementById("p_Connection_Status")
    let input_Address = document.getElementById("inp_sellerAccount")
    // Modern dapp browsers...
    if (window.ethereum) {
        web3Provider = window.ethereum;
        try {
            // Request account access
            accounts = await ethereum.request({ method: 'eth_requestAccounts' });
            const account = accounts[0];
            window.web3 = new Web3(web3Provider);
            btn_Connect.textContent= account;
            input_Address.value = account;
            let formRegister = document.getElementById("frm_register");
            formRegister.addEventListener("submit", function (event) {
                event.preventDefault();
                console.log(formRegister)
            })
            await initContract();
            await getItemCode();

        } catch (error) {
            // User denied account access...
            console.error("User denied account access")
            console.error(error)
            p_Connection_Status.textContent="User denied account access"
        }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
        web3Provider = window.web3.currentProvider;
        window.web3 = new Web3(web3Provider);
        alertModal("Metamask Connection Error",error.toString())
    }
};

async function  getItemCode() {
    try {
        let safePurchaseInstance = await contracts.SafePurchase.deployed();
        let lastItemCode = await safePurchaseInstance.purchaseCounter.call();
        let input_ItemCode = document.getElementById("inp_itemCode");
        input_ItemCode.value = lastItemCode;
    }
    catch (error) {
        alertModal("Error",error.toString())
    }

}

async function registerPurchase(){
    try {
        let input_ItemCode = document.getElementById("inp_itemCode")
        let inp_value = document.getElementById("inp_value")
        let safePurchaseInstance = await contracts.SafePurchase.deployed();
        const account = accounts[0];
        let res = await safePurchaseInstance.registerPurchase(input_ItemCode.value.toString(),{ from: account ,value:  window.web3.utils.toWei(inp_value.value.toString(), 'ether') });
        let formRegister = document.getElementById("frm_register");
        formRegister.method="post";
        formRegister.submit();
    }
    catch(error){
        alertModal("Error",error.toString())
    }

}

function alertModal(title, body) {
    // Display error message to the user in a modal
    $('#alert-modal-title').html(title);
    $('#alert-modal-body').html(body);
    $('#alert-modal').modal('show');
}


async function  getNetwork(networkCaption){
    const chainId = await ethereum.request({ method: 'eth_chainId' });
    ethereum.on('chainChanged', handleChainChanged);
    switch(chainId)
    {
        case 1:
            networkCaption.textContent = 'Ethereum'
        case 56:
            networkCaption.textContent = 'BSC'
        case 1337:
            networkCaption.textContent = 'Ganache'

    }
}

function handleChainChanged(_chainId) {
    window.location.reload();
}




