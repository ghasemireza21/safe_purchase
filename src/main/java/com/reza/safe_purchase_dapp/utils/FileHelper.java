package com.reza.safe_purchase_dapp.utils;

import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

public class FileHelper {

    public static String copyImageToResource(MultipartFile multipartFile) throws IOException {
        String path = ResourceUtils.getFile("classpath:static/img").getAbsolutePath();
        byte[] bytes = multipartFile.getBytes();
        String name = UUID.randomUUID() + "." + Objects.requireNonNull(multipartFile.getContentType()).split("/")[1];
        Files.write(Paths.get(path + File.separator + name), bytes);
        return name;
    }
}
