package com.reza.safe_purchase_dapp;

import com.reza.safe_purchase_dapp.purchase.wrapper.SafePurchaseContract;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.web3j.crypto.*;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.RemoteFunctionCall;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import org.web3j.tuples.generated.Tuple4;

import java.math.BigInteger;

@SpringBootApplication
public class SafePurchaseDappApplication {

    private final static BigInteger GAS_LIMIT = BigInteger.valueOf(6721975L);
    private final static BigInteger GAS_PRICE = BigInteger.valueOf(20000000000L);

    public static void main(String[] args) {
        SpringApplication.run(SafePurchaseDappApplication.class, args);

    }


}


