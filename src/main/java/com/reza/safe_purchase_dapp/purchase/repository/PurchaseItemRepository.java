package com.reza.safe_purchase_dapp.purchase.repository;

import com.reza.safe_purchase_dapp.purchase.model.PurchaseItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseItemRepository  extends JpaRepository<PurchaseItem,Long> {


    @Query("select p from PurchaseItem p where (:name is null or p.name like %:name% ) ")
    Page<PurchaseItem> findAllPurchaseItem(@Param("name") String purchaseItemName, Pageable pageable);

    PurchaseItem findByCode(int code);
}
