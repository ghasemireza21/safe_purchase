package com.reza.safe_purchase_dapp.purchase.service;

import com.reza.safe_purchase_dapp.purchase.model.PurchaseItem;
import com.reza.safe_purchase_dapp.purchase.model.State;
import com.reza.safe_purchase_dapp.purchase.repository.PurchaseItemRepository;
import com.reza.safe_purchase_dapp.purchase.wrapper.SafePurchaseContract;
import com.reza.safe_purchase_dapp.utils.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.tuples.generated.Tuple4;

import java.io.IOException;
import java.math.BigInteger;
import java.time.LocalDateTime;

@Service
public class PurchaseItemService {


    private PurchaseItemRepository purchaseItemRepository;
    private Web3j web3j;

    private final static BigInteger GAS_LIMIT = BigInteger.valueOf(6721975L);

    @Autowired
    public PurchaseItemService(PurchaseItemRepository purchaseItemRepository,Web3j web3j) {
        this.purchaseItemRepository = purchaseItemRepository;
        this.web3j = web3j;
    }

    public Page<PurchaseItem> findAllPurchaseItems(PurchaseItem purchaseItem, Pageable pageable){
        return purchaseItemRepository.findAllPurchaseItem(purchaseItem.getName(),pageable);
    }

    public PurchaseItem registerPurchase(PurchaseItem purchaseItem) throws IOException {

        if(confirmTransaction(purchaseItem.getCode(),0)) {
            if (!purchaseItem.getFile().isEmpty()) {
                purchaseItem.setCover(FileHelper.copyImageToResource(purchaseItem.getFile()));
            }
            LocalDateTime localDateTime = LocalDateTime.now();
            purchaseItem.setCreateDateTime(localDateTime);
            State state = new State();
            state.setId(0);
            //purchaseItem.setCode(1);
            purchaseItem.setState(state);
            purchaseItem.setValue((purchaseItem.getValue() / 2));
            return purchaseItemRepository.save(purchaseItem);
        }
        return null;
    }

    @Transactional
    public void confirmPurchase(int code,String buyerAddress) {
        if(confirmTransaction(code,1)) {
            PurchaseItem purchaseItem = purchaseItemRepository.findByCode(code);
            State state = new State();
            state.setId(1);
            purchaseItem.setState(state);
            purchaseItem.setBuyerAddress(buyerAddress);
            purchaseItemRepository.save(purchaseItem);
        }
    }

    @Transactional
    public void confirmReceived(int code) {
        if(confirmTransaction(code,2)) {
            PurchaseItem purchaseItem = purchaseItemRepository.findByCode(code);
            State state = new State();
            state.setId(2);
            purchaseItem.setState(state);
            purchaseItemRepository.save(purchaseItem);
        }
    }

    @Transactional
    public void confirmAbortOrRefund(int code) {
        if(confirmTransaction(code,3)) {
            PurchaseItem purchaseItem = purchaseItemRepository.findByCode(code);
            State state = new State();
            state.setId(3);
            purchaseItem.setState(state);
            purchaseItemRepository.save(purchaseItem);
        }
    }

    public  boolean confirmTransaction(int purchaseId, int newState) {
        try {
            try {
                Web3ClientVersion clientVersion = web3j.web3ClientVersion().sendAsync().get();

                BigInteger gasPrice =web3j.ethGasPrice().send().getGasPrice();



                if (!clientVersion.hasError()) {
                    //Connected
                    Credentials credentials =
                            Credentials.create("a809dc0c22a8fc9c173df60df9885c77d9580023e682a01f7b2f57046d10af96");
                    //ContractGasProvider contractGasProvider = new DefaultGasProvider();

                    String ContractAddress = "0xE68f250762CF0EAd50A34b59514aC32f85F068D2";

                    SafePurchaseContract safePurchaseContract = SafePurchaseContract.load(ContractAddress, web3j,
                            credentials
                            , gasPrice, GAS_LIMIT);
                    Tuple4<BigInteger, String, String, BigInteger> tuple4 = safePurchaseContract.getPurchase(BigInteger.valueOf(purchaseId)).send();
                    //TransactionReceipt transactionReceipt;
                    //transactionReceipt =  safePurchaseContract.registerPurchase(BigInteger.valueOf(3)).send();
                    //System.out.print(tuple4RemoteFunctionCall.encodeFunctionCall());
                    //Tuple4<BigInteger, String, String, BigInteger> tuple4 =tuple4RemoteFunctionCall.send();
                    return (BigInteger.valueOf(newState).equals(tuple4.component4()) ) ? true:false;
                } else {
                    System.out.print("Notconnected");
                    //Show Error
                }
            } catch (Exception e) {
                System.out.print(e.getMessage());
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return false;
    }

}
