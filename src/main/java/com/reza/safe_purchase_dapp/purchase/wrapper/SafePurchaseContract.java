package com.reza.safe_purchase_dapp.purchase.wrapper;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint8;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.RemoteFunctionCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.BaseEventResponse;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tuples.generated.Tuple4;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 4.5.16.
 */
@SuppressWarnings("rawtypes")
public class SafePurchaseContract extends Contract {
    public static final String BINARY = "60806040526001805534801561001457600080fd5b50610818806100246000396000f3fe60806040526004361061007b5760003560e01c80634d24e9021161004e5780634d24e9021461012e5780636537e0b7146101415780638392fe3114610161578063e50701f4146101c657600080fd5b8063055680f01461008057806327dac36d146100a95780633742a9f7146100cb5780634036aaf01461011b575b600080fd5b34801561008c57600080fd5b5061009660015481565b6040519081526020015b60405180910390f35b3480156100b557600080fd5b506100c96100c43660046106f6565b6101e6565b005b3480156100d757600080fd5b506100eb6100e63660046106f6565b6102fd565b6040516100a094939291909384526001600160a01b03928316602085015291166040830152606082015260800190565b6100c96101293660046106f6565b61035a565b6100c961013c3660046106f6565b61040b565b34801561014d57600080fd5b506100c961015c3660046106f6565b6104dc565b34801561016d57600080fd5b506101b661017c3660046106f6565b60006020819052908152604090208054600182015460029092015490916001600160a01b039081169190811690600160a01b900460ff1684565b6040516100a09493929190610725565b3480156101d257600080fd5b506100c96101e13660046106f6565b6105fd565b60008181526020819052604090206002015481906001600160a01b03163314610222576040516386efbb5560e01b815260040160405180910390fd5b60018281600082815260208190526040902060020154600160a01b900460ff1660038111156102535761025361070f565b146102715760405163baf3f0f760e01b815260040160405180910390fd5b6040517fe89152acd703c9d8c7d28829d443260b411454d45394e7995815140c8cbcbcf790600090a1600084815260208190526040808220600281018054600160a11b60ff60a01b19821617909155905491516001600160a01b03909116926108fc831502929190818181858888f193505050501580156102f6573d6000803e3d6000fd5b5050505050565b6000818152602081905260408120805460028201546001830154849384938493919290916001600160a01b03808216921690600160a01b900460ff16600381111561034a5761034a61070f565b9450945094509450509193509193565b60008181526020819052604090206001015481906001600160a01b03161561039557604051633e04f87160e01b815260040160405180910390fd5b60008281526020819052604090206001810180546001600160a01b031916331790556103c2600234610786565b8155600180549060006103d4836107a8565b9091555050805434906103e89060026107c3565b1461040657604051635f1f262160e11b815260040160405180910390fd5b505050565b60008181600082815260208190526040902060020154600160a01b900460ff16600381111561043c5761043c61070f565b1461045a5760405163baf3f0f760e01b815260040160405180910390fd5b6000838152602081905260409020546104749060026107c3565b34148061048057600080fd5b6040517fd5d55c8a68912e9a110618df8d5e2e83b8d83211c57a8ddd1203df92885dc88190600090a1505050600090815260208190526040902060020180546001600160a81b0319163360ff60a01b191617600160a01b179055565b60008181526020819052604090206001015481906001600160a01b03163314610518576040516342e8fb9360e11b815260040160405180910390fd5b60028281600082815260208190526040902060020154600160a01b900460ff1660038111156105495761054961070f565b146105675760405163baf3f0f760e01b815260040160405180910390fd5b6040517ffda69c32bcfdba840a167777906b173b607eb8b4d8853b97a80d26e613d858db90600090a1600084815260208190526040902060028101805460ff60a01b1916600360a01b179055600181015490546001600160a01b03909116906108fc906105d59060036107c3565b6040518115909202916000818181858888f193505050501580156102f6573d6000803e3d6000fd5b60008181526020819052604090206001015481906001600160a01b03163314610639576040516342e8fb9360e11b815260040160405180910390fd5b60008281600082815260208190526040902060020154600160a01b900460ff16600381111561066a5761066a61070f565b146106885760405163baf3f0f760e01b815260040160405180910390fd5b6040517f72c874aeff0b183a56e2b79c71b46e1aed4dee5e09862134b8821ba2fddbf8bf90600090a160008481526020819052604090206002808201805460ff60a01b1916600360a01b179055600182015491546001600160a01b03909216916108fc916105d591906107c3565b60006020828403121561070857600080fd5b5035919050565b634e487b7160e01b600052602160045260246000fd5b8481526001600160a01b03848116602083015283166040820152608081016004831061076157634e487b7160e01b600052602160045260246000fd5b82606083015295945050505050565b634e487b7160e01b600052601160045260246000fd5b6000826107a357634e487b7160e01b600052601260045260246000fd5b500490565b60006000198214156107bc576107bc610770565b5060010190565b60008160001904831182151516156107dd576107dd610770565b50029056fea2646970667358221220eb41d053f92b6134743c8b48ad95c0020f4a21cef32517ac2b0dd03c03d6ea0d64736f6c63430008090033";

    public static final String FUNC_ABORT = "abort";

    public static final String FUNC_CONFIRMPURCHASE = "confirmPurchase";

    public static final String FUNC_CONFIRMRECEIVED = "confirmReceived";

    public static final String FUNC_GETPURCHASE = "getPurchase";

    public static final String FUNC_PURCHASECOUNTER = "purchaseCounter";

    public static final String FUNC_PURCHASES = "purchases";

    public static final String FUNC_REFUNDSELLER = "refundSeller";

    public static final String FUNC_REGISTERPURCHASE = "registerPurchase";

    public static final Event ABORTED_EVENT = new Event("Aborted", 
            Arrays.<TypeReference<?>>asList());
    ;

    public static final Event ITEMRECEIVED_EVENT = new Event("ItemReceived", 
            Arrays.<TypeReference<?>>asList());
    ;

    public static final Event PURCHASECONFIRMED_EVENT = new Event("PurchaseConfirmed", 
            Arrays.<TypeReference<?>>asList());
    ;

    public static final Event SELLERREFUNDED_EVENT = new Event("SellerRefunded", 
            Arrays.<TypeReference<?>>asList());
    ;

    @Deprecated
    protected SafePurchaseContract(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected SafePurchaseContract(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected SafePurchaseContract(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected SafePurchaseContract(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public List<AbortedEventResponse> getAbortedEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(ABORTED_EVENT, transactionReceipt);
        ArrayList<AbortedEventResponse> responses = new ArrayList<AbortedEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            AbortedEventResponse typedResponse = new AbortedEventResponse();
            typedResponse.log = eventValues.getLog();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<AbortedEventResponse> abortedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, AbortedEventResponse>() {
            @Override
            public AbortedEventResponse apply(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(ABORTED_EVENT, log);
                AbortedEventResponse typedResponse = new AbortedEventResponse();
                typedResponse.log = log;
                return typedResponse;
            }
        });
    }

    public Flowable<AbortedEventResponse> abortedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(ABORTED_EVENT));
        return abortedEventFlowable(filter);
    }

    public List<ItemReceivedEventResponse> getItemReceivedEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(ITEMRECEIVED_EVENT, transactionReceipt);
        ArrayList<ItemReceivedEventResponse> responses = new ArrayList<ItemReceivedEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            ItemReceivedEventResponse typedResponse = new ItemReceivedEventResponse();
            typedResponse.log = eventValues.getLog();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<ItemReceivedEventResponse> itemReceivedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, ItemReceivedEventResponse>() {
            @Override
            public ItemReceivedEventResponse apply(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(ITEMRECEIVED_EVENT, log);
                ItemReceivedEventResponse typedResponse = new ItemReceivedEventResponse();
                typedResponse.log = log;
                return typedResponse;
            }
        });
    }

    public Flowable<ItemReceivedEventResponse> itemReceivedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(ITEMRECEIVED_EVENT));
        return itemReceivedEventFlowable(filter);
    }

    public List<PurchaseConfirmedEventResponse> getPurchaseConfirmedEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(PURCHASECONFIRMED_EVENT, transactionReceipt);
        ArrayList<PurchaseConfirmedEventResponse> responses = new ArrayList<PurchaseConfirmedEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            PurchaseConfirmedEventResponse typedResponse = new PurchaseConfirmedEventResponse();
            typedResponse.log = eventValues.getLog();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<PurchaseConfirmedEventResponse> purchaseConfirmedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, PurchaseConfirmedEventResponse>() {
            @Override
            public PurchaseConfirmedEventResponse apply(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(PURCHASECONFIRMED_EVENT, log);
                PurchaseConfirmedEventResponse typedResponse = new PurchaseConfirmedEventResponse();
                typedResponse.log = log;
                return typedResponse;
            }
        });
    }

    public Flowable<PurchaseConfirmedEventResponse> purchaseConfirmedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(PURCHASECONFIRMED_EVENT));
        return purchaseConfirmedEventFlowable(filter);
    }

    public List<SellerRefundedEventResponse> getSellerRefundedEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(SELLERREFUNDED_EVENT, transactionReceipt);
        ArrayList<SellerRefundedEventResponse> responses = new ArrayList<SellerRefundedEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            SellerRefundedEventResponse typedResponse = new SellerRefundedEventResponse();
            typedResponse.log = eventValues.getLog();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<SellerRefundedEventResponse> sellerRefundedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, SellerRefundedEventResponse>() {
            @Override
            public SellerRefundedEventResponse apply(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(SELLERREFUNDED_EVENT, log);
                SellerRefundedEventResponse typedResponse = new SellerRefundedEventResponse();
                typedResponse.log = log;
                return typedResponse;
            }
        });
    }

    public Flowable<SellerRefundedEventResponse> sellerRefundedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(SELLERREFUNDED_EVENT));
        return sellerRefundedEventFlowable(filter);
    }

    public RemoteFunctionCall<TransactionReceipt> abort(BigInteger id) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_ABORT, 
                Arrays.<Type>asList(new Uint256(id)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> confirmPurchase(BigInteger id) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_CONFIRMPURCHASE, 
                Arrays.<Type>asList(new Uint256(id)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> confirmReceived(BigInteger id) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_CONFIRMRECEIVED, 
                Arrays.<Type>asList(new Uint256(id)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<Tuple4<BigInteger, String, String, BigInteger>> getPurchase(BigInteger id) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_GETPURCHASE, 
                Arrays.<Type>asList(new Uint256(id)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Address>() {}, new TypeReference<Address>() {}, new TypeReference<Uint256>() {}));
        return new RemoteFunctionCall<Tuple4<BigInteger, String, String, BigInteger>>(function,
                new Callable<Tuple4<BigInteger, String, String, BigInteger>>() {
                    @Override
                    public Tuple4<BigInteger, String, String, BigInteger> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);
                        return new Tuple4<BigInteger, String, String, BigInteger>(
                                (BigInteger) results.get(0).getValue(), 
                                (String) results.get(1).getValue(), 
                                (String) results.get(2).getValue(), 
                                (BigInteger) results.get(3).getValue());
                    }
                });
    }

    public RemoteFunctionCall<BigInteger> purchaseCounter() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_PURCHASECOUNTER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<Tuple4<BigInteger, String, String, BigInteger>> purchases(BigInteger param0) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_PURCHASES, 
                Arrays.<Type>asList(new Uint256(param0)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Address>() {}, new TypeReference<Address>() {}, new TypeReference<Uint8>() {}));
        return new RemoteFunctionCall<Tuple4<BigInteger, String, String, BigInteger>>(function,
                new Callable<Tuple4<BigInteger, String, String, BigInteger>>() {
                    @Override
                    public Tuple4<BigInteger, String, String, BigInteger> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);
                        return new Tuple4<BigInteger, String, String, BigInteger>(
                                (BigInteger) results.get(0).getValue(), 
                                (String) results.get(1).getValue(), 
                                (String) results.get(2).getValue(), 
                                (BigInteger) results.get(3).getValue());
                    }
                });
    }

    public RemoteFunctionCall<TransactionReceipt> refundSeller(BigInteger id) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_REFUNDSELLER, 
                Arrays.<Type>asList(new Uint256(id)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> registerPurchase(BigInteger id) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_REGISTERPURCHASE, 
                Arrays.<Type>asList(new Uint256(id)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    @Deprecated
    public static SafePurchaseContract load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new SafePurchaseContract(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static SafePurchaseContract load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new SafePurchaseContract(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static SafePurchaseContract load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new SafePurchaseContract(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static SafePurchaseContract load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new SafePurchaseContract(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static RemoteCall<SafePurchaseContract> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(SafePurchaseContract.class, web3j, credentials, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<SafePurchaseContract> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(SafePurchaseContract.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<SafePurchaseContract> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(SafePurchaseContract.class, web3j, transactionManager, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<SafePurchaseContract> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(SafePurchaseContract.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    public static class AbortedEventResponse extends BaseEventResponse {
    }

    public static class ItemReceivedEventResponse extends BaseEventResponse {
    }

    public static class PurchaseConfirmedEventResponse extends BaseEventResponse {
    }

    public static class SellerRefundedEventResponse extends BaseEventResponse {
    }
}
