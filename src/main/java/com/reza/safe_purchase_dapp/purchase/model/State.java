package com.reza.safe_purchase_dapp.purchase.model;

import org.hibernate.annotations.Nationalized;

import javax.persistence.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Table(name = "b_state")
public class State {

    @Id
    @Column(name = "pk_state_id")
    private int id;

    @Nationalized
    @Column(name = "state")
    @NotBlank
    private String state;

    @OneToMany(mappedBy = "state")
    private List<PurchaseItem> purchaseItems;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<PurchaseItem> getPurchaseItems() {
        return purchaseItems;
    }

    public void setPurchaseItems(List<PurchaseItem> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }
}
