package com.reza.safe_purchase_dapp.purchase.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Nationalized;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "m_purchase_item")

public class PurchaseItem {
    @Id
    @GeneratedValue()
    @Column(name = "pk_purchase_item_id")
    private long id;

    @Column(name = "code",unique = true)
    private int code;

    @Nationalized
    @NotBlank
    @Column(name="name")
    private String name;

    @Nationalized
    @NotBlank
    @Column(name="brand")
    private String brand;


    @Column(name = "cover")
    private String Cover;

    @Column(name = "value")
    private Double value;


    @NotBlank
    @Column(name="seller_address")
    @NotNull
    private String sellerAddress;

    @Column(name="buyer_address")
    private String buyerAddress;

    @Column(name="dt_create")
    private LocalDateTime createDateTime;

    @Column(name = "dt_sell")
    private LocalDateTime sellDateTime;

    @Column(name = "dt_update")
    private LocalDateTime lastUpdateTime;

    @ManyToOne
    @JoinColumn(name = "fk_state_id")
    private State state;

    @Transient
    @JsonIgnore
    private MultipartFile file;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCover() {
        return Cover;
    }

    public void setCover(String cover) {
        Cover = cover;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getSellerAddress() {
        return sellerAddress;
    }

    public void setSellerAddress(String sellerAddress) {
        this.sellerAddress = sellerAddress;
    }

    public String getBuyerAddress() {
        return buyerAddress;
    }

    public void setBuyerAddress(String buyerAddress) {
        this.buyerAddress = buyerAddress;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDateTime getSellDateTime() {
        return sellDateTime;
    }

    public void setSellDateTime(LocalDateTime sellDateTime) {
        this.sellDateTime = sellDateTime;
    }

    public LocalDateTime getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(LocalDateTime lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
