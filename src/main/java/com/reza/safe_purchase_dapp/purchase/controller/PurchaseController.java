package com.reza.safe_purchase_dapp.purchase.controller;

import com.reza.safe_purchase_dapp.purchase.model.PurchaseItem;
import com.reza.safe_purchase_dapp.purchase.service.PurchaseItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import javax.validation.Valid;
import java.io.IOException;

@Controller
@RequestMapping("/")
public class PurchaseController {

    private PurchaseItemService purchaseItemService;

    @Autowired
    public PurchaseController(PurchaseItemService purchaseItemService) {
        this.purchaseItemService = purchaseItemService;
    }

    @RequestMapping(value = {"","/index"},method = RequestMethod.GET)
    public String getPurchaseItem(@ModelAttribute("p") PurchaseItem purchaseItem, Model model, @PageableDefault(size = 20) Pageable pageable) {
        model.addAttribute("purchaseItem", purchaseItemService.findAllPurchaseItems(purchaseItem, pageable));
        model.addAttribute("PageNavigationFormatter","%s از %s");
        //model.addAttribute("CategoriesGroup",categoriesService.findCategoriesPostCount());
        //model.addAttribute("Tags",tagsService.findAllTags());
        return "index";
    }

    @RequestMapping(value = "/register",method = RequestMethod.GET)
    public String registerPage(Model model){
        model.addAttribute("purchaseItem",new PurchaseItem());
        return "register_Item";
    }

    @RequestMapping(value = "/register",method = RequestMethod.POST)
    public String registerItem(@ModelAttribute(name = "purchaseItem") @Valid  PurchaseItem Item, BindingResult bindingResult,Model model) throws IOException {
        if (bindingResult.hasErrors())
            return "register_Item";
        purchaseItemService.registerPurchase(Item);
        return "redirect:/";
    }

    @RequestMapping(value = "/purchase/{code}/{address}", method = RequestMethod.GET)
    public String confirmPurchase(@PathVariable(value = "code") int code,@PathVariable(value = "address") String buyerAddress){
        purchaseItemService.confirmPurchase(code,buyerAddress);
        return "redirect:/index";
    }

    @RequestMapping(value = "/received/{code}", method = RequestMethod.GET)
    public String confirmReceived(@PathVariable(value = "code") int code){
        purchaseItemService.confirmReceived(code);
        return "redirect:/index";
    }

    @RequestMapping(value = "/refund/{code}", method = RequestMethod.GET)
    public String confirmRefund(@PathVariable(value = "code") int code){
        purchaseItemService.confirmAbortOrRefund(code);
        return "redirect:/index";
    }

    @RequestMapping(value = "/refundAbort/{code}", method = RequestMethod.GET)
    public String confirmAbort(@PathVariable(value = "code") int code){
        purchaseItemService.confirmAbortOrRefund(code);
        return "redirect:/index";
    }

}
